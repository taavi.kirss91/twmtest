import { IArticle } from "../types/interfaces/article.interface";
import { AbstractService } from "./abstract.service";

export class ArticleService extends AbstractService<IArticle>{

  public getArticle(): Promise<IArticle> {
    return this.sendGET('http://proovitoo.twn.ee/api/article.json');
  }
}
