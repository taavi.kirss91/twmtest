import { IList } from "../types/interfaces/list.interface";
import { AbstractService } from "./abstract.service";

export class ListService extends AbstractService<IList>{

  public getList(): Promise<IList> {
    return this.sendGET('http://proovitoo.twn.ee/api/list.json');
  }
}
