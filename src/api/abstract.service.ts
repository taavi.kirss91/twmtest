

export class AbstractService<T = any> {

  async throwHttpException(response: Response): Promise<Response> {
    const error = await response?.json();

    if (!error) {
      throw new Error(response.statusText);
    }
    throw error;
  }

  private validateResponse = async (response: Response): Promise<Response> => {
    if (response.ok) {
      return response;
    }
    return this.throwHttpException(response);
  };

  parseResponse = (response: Response) => response.json();

  handleError = (error: any) => {
    console.log('handleError', error);
    throw error;
  };

  sendGET<Response = T>(url: string): Promise<Response> {
    return fetch(url)
      .then(this.validateResponse)
      .then(this.parseResponse)
      .catch(this.handleError)
  }
}
