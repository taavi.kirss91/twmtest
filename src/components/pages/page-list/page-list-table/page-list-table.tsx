import React from "react";
import { stripTags, truncate } from "../../../../assets/helpers/filters";
import { ESortRule } from "../../../../types/enums/sort-rule-dir.enum";
import { ESortKey } from "../../../../types/enums/sort-rule-key.enum";
import { IListItem } from "../../../../types/interfaces/list.interface";
import "./page-list-table.scss";

export interface ITable {
  displayRows: number,
}

export class PageTable extends React.Component<ITable> {
  state: {
    listData: IListItem[] | null;
    displayRows: number;
    orderRule: {
      key: string;
      rule: ESortRule
    } | null;
    selectedRow: number | null;
    selectedPage: number;
  }

  constructor(props: any) {
    super(props as any);
    this.state = {
      listData: null,
      displayRows: this.props.displayRows,
      orderRule: null,
      selectedRow: null,
      selectedPage: 1,
    }
  }

  componentDidMount(): void {
    const data = JSON.parse(sessionStorage.getItem("twmList") || '');
    this.setState({ listData: data.splice(0, this.state.displayRows) }) // use splice to get first page rows
  }

  private getCaptions: () => { label: string; key: string; rule?: ESortRule }[] = () => [
    {
      label: "Eesnimi",
      key: ESortKey.NAME,
      rule: (this.state.orderRule?.key === ESortKey.NAME && this.state.orderRule.rule) || ESortRule.NONE
    },
    {
      label: "Perekonnanimi",
      key: ESortKey.SURNAME,
      rule: (this.state.orderRule?.key === ESortKey.SURNAME && this.state.orderRule.rule) || ESortRule.NONE
    },
    {
      label: "Sugu",
      key: ESortKey.SEX,
      rule: (this.state.orderRule?.key === ESortKey.SEX && this.state.orderRule.rule) || ESortRule.NONE
    },
    {
      label: "Sünnikuupäev",
      key: ESortKey.DATE,
      rule: (this.state.orderRule?.key === ESortKey.DATE && this.state.orderRule.rule) || ESortRule.NONE
    },
    {
      label: "Telefon",
      key: ESortKey.PHONE,
      rule: (this.state.orderRule?.key === ESortKey.PHONE && this.state.orderRule.rule) || ESortRule.NONE
    },
  ]

  sortDates(a: IListItem, b: IListItem): number {
    // get entry's birthday in string format
    const elA = this.getBirthDay((a as IListItem).personal_code).split(".");
    const elB = this.getBirthDay((b as IListItem).personal_code).split(".");

    // create new timestamp from birthday string
    const dateA = new Date(Number(elA[0]), Number(elA[1]), Number(elA[2])).getTime();
    const dateB = new Date(Number(elB[0]), Number(elB[1]), Number(elB[2])).getTime();

    // compare timestamps
    return dateA - dateB;
  }

  sortListData(list: IListItem[]) {
    // Need proper locale for Estonian alphabet
    const collator = new Intl.Collator('et');

    const sortedList = list.sort((a: any, b: any) => {
      if (!this.state.orderRule) {
        return 0
      }

      let order: number = 0;

      if (this.state.orderRule.key === ESortKey.DATE) {
        order = this.sortDates(a, b)
      } else {
        order = collator.compare(a[this.state.orderRule.key], b[this.state.orderRule.key]);
      }
      if (this.state.orderRule?.rule === ESortRule.DESC) {
        order = order * (-1);
      }
      return order;
    });
    return sortedList
  }

  setListOrder() {
    const listCopy = JSON.parse(sessionStorage.getItem("twmList") || '');

    if (this.state.orderRule?.rule === ESortRule.NONE) {
      this.setState({ listData: listCopy })
      return;
    }

    this.setState({ listData: this.sortListData(listCopy) })
  }

  setListOrderRule(key: string, captions: any) {
    const currentSort = captions.find((r: any) => r.key === key).rule;
    this.setState({
      orderRule: {
        key,
        rule: currentSort === ESortRule.ASC
          ? ESortRule.DESC
          : currentSort === ESortRule.DESC
            ? ESortRule.NONE
            : ESortRule.ASC
      }
    }, this.setListOrder
    )
  }

  handleCaptionClick(key: string) {
    if (!key) {
      return;
    }
    this.setState({ selectedRow: null });
    this.setListOrderRule(key, this.getCaptions());
  }

  getSortIcon(caption: any): string {
    const currentSort = this.state.orderRule;
    if (!currentSort || caption.key !== currentSort.key || currentSort.rule === ESortRule.NONE) {
      return "page-list-table__th-icon--inactive";
    }
    if (!!currentSort && currentSort.key === caption.key && currentSort.rule === ESortRule.ASC) {
      return "page-list-table__th-icon--ascending";
    }
    if (!!currentSort && currentSort.key === caption.key && currentSort.rule === ESortRule.DESC) {
      return "page-list-table__th-icon--descending";
    }
    return "";
  }

  getHeader() {
    return (
      <thead className="page-list-table__thead">
        <tr>
          {this.getCaptions().map((caption, index) => (
            <th key={index} className="page-list-table__th" onClick={() => this.handleCaptionClick(caption.key)}>
              {caption.label}
              <i className={`page-list-table__th-icon ${this.getSortIcon(caption)}`} />
            </th>
          ))}
        </tr>
      </thead>
    )
  }

  getBirthDay(value: number): string {
    // data.date does not seem to indicate date of birth
    // possible errors in original data?
    // currently get bdate from id code
    const data = value.toString()

    const controlNr = Number(data[0]);
    const date = data.slice(1, 7)

    let year = date.slice(0, 2);
    const month = date.slice(2, 4);
    const day = date.slice(4, 6);

    if (controlNr < 5) {
      year = '19' + date.slice(0, 2);
    } else {
      year = '20' + date.slice(0, 2);
    }
    return year + "." + month + "." + day;
  }

  getSex(value: string): string {
    switch (value) {
      case "f": {
        return "Naine"
      }
      case "m": {
        return "Mees"
      }
      default: {
        return ""
      }
    }
  }

  getColumns(data: IListItem) {
    return [
      {
        renderer: () => {
          return (
            <p>{JSON.parse('"' + data.firstname + '"')}</p>
          )
        }
      },
      {
        renderer: () => {
          return (
            <p>{JSON.parse('"' + data.surname + '"')}</p>
          )
        }
      },
      {
        renderer: () => {
          return (
            <p>{this.getSex(data.sex)}</p>
          )
        }
      },
      {
        renderer: () => {
          return (
            <p>{this.getBirthDay(data.personal_code)}</p>
          )
        }
      },
      {
        renderer: () => {
          return (
            <p>{data.phone}</p>
          )
        }
      },
    ]
  }

  getRows() {
    if (!this.state.listData?.length) {
      return [];
    }
    const limit = this.state.displayRows;

    const startIndex = this.state.selectedPage === 1 ? 0 : ((this.state.selectedPage - 1) * limit);
    const stopIndex = this.state.selectedPage === 1 ? limit : ((this.state.selectedPage - 1) * limit) + limit;

    return this.state.listData.map((entry) => {
      return { cells: this.getColumns(entry) }
    }).filter(Boolean).slice(startIndex, stopIndex); // use slice to not mutate state
  }

  selectRow(index: number) {
    if (this.state.selectedRow === index) {
      this.setState({ selectedRow: null })
      return
    }
    this.setState({ selectedRow: index });
  }

  displayExtraRow(index: number) {
    if (!this.state.listData) {
      return;
    }
    const data = this.state.listData[index]

    return (
      <div className="page-list-table__tr--show-content">
        <img src={data.images[0]['small']} alt={data.images[0]['alt']} />
        <p>{truncate(stripTags(data.intro), 285)}</p>
      </div>
    )
  }

  getCells() {
    return (
      <tbody>
        {this.getRows().map((row, index) => {
          return (
            <React.Fragment key={index}>
              <tr
                key={index}
                className={
                  `page-list-table__tr ${index === this.state.selectedRow
                    ? "page-list-table__tr--active"
                    : ""}`
                }
                onClick={() => this.selectRow(index)}
              >
                {
                  row.cells.map((cell, index) => (
                    <td key={index} className="page-list-table__tr__td">
                      {cell.renderer()}
                    </td>
                  ))
                }
              </tr>
              {this.state.selectedRow === index &&
                <tr className="page-list-table__tr--show">
                  <td colSpan={10}>
                    {this.displayExtraRow(index)}
                  </td>
                </tr>
              }
            </React.Fragment>
          )
        }
        )}
      </tbody>
    )
  }

  selectPage(pageNumber: number) {
    this.setState({ selectedRow: null });
    if (this.state.selectedPage !== pageNumber) {
      this.setState({ selectedPage: pageNumber }, this.setListOrder)
    }
  }

  changePageNumber(value: number, pages: number): void {
    this.setState({ selectedRow: null });
    const currentPage = this.state.selectedPage;
    const newValue = currentPage + value;
    if (newValue === 0 || newValue > pages) {
      return;
    }
    this.setState({ selectedPage: newValue }, this.setListOrder);
  }

  getPageIcons(pages: number) {
    let pageIcons = [];
    for (let page = 1; page <= pages; page++) {
      pageIcons.push(
        <span
          className={
            `page-list-table__pagination__page ${page === this.state.selectedPage
              ? "page-list-table__pagination__page--active"
              : ""}`
          }
          key={page}
          onClick={() => this.selectPage(page)}
        >
          {page}
        </span>
      )
    }
    return pageIcons
  }

  getPaginator() {
    if (!this.state.listData) {
      return;
    }
    const totalResults = JSON.parse(sessionStorage.getItem("twmList") || '').length;
    const displayRows = this.state.displayRows;
    const pages = Math.round(totalResults / displayRows);

    const currentPage = this.state.selectedPage;

    const startIndex = currentPage < 4 ? 0 : currentPage > pages - 3 ? pages - 5 : currentPage - 3;
    const stopIndex = currentPage < 4 ? 5 : currentPage > pages - 3 ? pages : currentPage + 2;

    return (
      <div className="page-list-table__pagination">
        <span
          className="page-list-table__pagination__left"
          onClick={() => this.changePageNumber(-1, pages)}
        >
          {"<"}
        </span>
          {this.getPageIcons(pages).slice(startIndex, stopIndex)}
        <span
          className="page-list-table__pagination__right"
          onClick={() => this.changePageNumber(1, pages)}
        >
          {">"}
        </span>
      </div>
    )
  }

  render() {
    return (
      <>
        <div className="page-list-table">
          <table className="page-list-table__content">
            {this.getHeader()}
            {this.getCells()}
          </table>
        </div>
        {this.getPaginator()}
      </>
    )
  }
}
