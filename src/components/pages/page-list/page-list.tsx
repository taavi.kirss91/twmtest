import React from "react";
import { DI } from "../../../DI/DI";
import { IList } from "../../../types/interfaces/list.interface";
import { PageTable } from "./page-list-table/page-list-table";
import "./page-list.scss"

export class ListPage extends React.Component {
  private listService = DI.getListService();

  state: {
    loading: boolean;
  };

  constructor(props: any) {
    super(props as any);
    this.state = {
      loading: true,
    }
  }

  componentDidMount(): void {
    this.checkListData();
  }

  async getListData() {
    return await this.listService.getList() as IList;
  }

  requestListData() {
    this.getListData()
      .then(res => {
        // store data in sessionStorage to make query once per session
        sessionStorage.setItem("twmList", JSON.stringify(res.list))
        this.setState({
          loading: false
        });
      })
      .catch(err => {
        throw new Error(err)
      })
  }

  checkListData() {
    const list = sessionStorage.getItem("twmList");
    if (!!list && !!list.length) {
      this.setState({
        loading: false
      });
    } else {
      this.requestListData();
    }
  }

  render() {
    if (!!this.state.loading) {
      return null;
    }
    return (
      <div className="page-list">
        <h2>Nimekiri</h2>
        <PageTable displayRows={10} />
      </div>
    );
  }
}
