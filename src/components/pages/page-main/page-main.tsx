import React from "react";
import { routes } from "../../../router/index";
import { PageMenu } from "./page-main-menu/page-main-menu";
import { Switch, Route, Redirect } from "react-router-dom";
import "./page-main.scss";

export class PageMain extends React.Component {

  render() {
    return (
      <div className="page-main">
        <div className="page-main__wrapper">
          <PageMenu />
          <div className="page-main__content">
            <Switch>
              {routes.map((route, i) => (
                <Route key={i} {...route} />
              ))}
              <Route exact path="/">
                <Redirect to="/list" />
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}
