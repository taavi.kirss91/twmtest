import React from "react";
import Logo from "../../../../assets/img/logo.svg";
import { NavLink } from "react-router-dom";
import "./page-main-menu.scss";

export class PageMenu extends React.Component {

	render() {
		return (
			<div className="page-menu">
				<a className="page-menu__logo" href="/">
					<img src={Logo} alt="" />
					<i>proovitöö</i>
				</a>
				<ul className="page-menu__list">
					<li className="page-menu__list-item">
						<NavLink
							className="page-menu__list-item__link"
							activeClassName="page-menu__list-item__link--selected"
							to="/list"
						>
							Nimekiri
						</NavLink>
					</li>
					<li className="page-menu__list-item">
						<NavLink
							className="page-menu__list-item__link"
							activeClassName="page-menu__list-item__link--selected"
							to="/article"
						>
							Artikkel
						</NavLink>
					</li>
				</ul>
			</div>
		);
	}
}
