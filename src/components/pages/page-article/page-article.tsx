/* eslint-disable no-useless-escape */
import React from "react";
import { DI } from "../../../DI/DI";
import { stripTags } from "../../../assets/helpers/filters";
import { IArticle } from "../../../types/interfaces/article.interface";
import "./page-article.scss";

export class ArticlePage extends React.Component {
  private articleService = DI.getArticleService();
  state: {
    loading: boolean;
  };

  constructor(props: any) {
    super(props as any);
    this.state = {
      loading: true,
    }
  }

  componentDidMount(): void {
    this.checkArticleData();
  }

  async getArticleData() {
    return await this.articleService.getArticle() as IArticle;
  }

  requestArticleData() {
    this.getArticleData()
      .then(res => {
        sessionStorage.setItem('twmArticle', JSON.stringify(res))
        this.setState({
          loading: false
        });
      })
      .catch(err => {
        throw new Error(err)
      })
  }

  checkArticleData() {
    const article = sessionStorage.getItem('twmArticle');
    if (!!article) {
      this.setState({
        loading: false
      });
    } else {
      this.requestArticleData();
    }
  }

  getImage(val: string, data: IArticle): string {
    if (!data.images.length) {
      return ''
    }
    return data.images[0][val]
  }

  render() {
    if (!!this.state.loading) {
      return null;
    }
    const articleData: IArticle = JSON.parse(sessionStorage.getItem('twmArticle') || "")
    return (
      <div className="page-article">
        <h2>{articleData?.title}</h2>
        <h3>{stripTags(articleData?.intro)}</h3>
        <img src={this.getImage("medium", articleData)} alt={this.getImage("alt", articleData)} />
        <p>
          {articleData?.body?.split("<\/p>").map((value, index) => {
            return (
              <span key={index}>
                {stripTags(value)}
                <br />
                <br />
              </span>
            )
          })}
        </p>
        <div className="page-article__footer">
          {
            articleData?.tags?.map((tag, index) => (
              <div key={index} className="page-article__footer__tag">{tag}</div>
            ))
          }
        </div>
      </div>
    );
  }
}
