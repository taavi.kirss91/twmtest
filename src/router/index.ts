import { ListPage } from "../components/pages/page-list/page-list";
import { ArticlePage } from "../components/pages/page-article/page-article";

export const routes = [
  {
    path: "/list",
    component: ListPage
  },
  {
    path: "/article",
    component: ArticlePage
  },
]
