export enum ESortRule {
  ASC = "asc",
  DESC = "desc",
  NONE = "none",
}
