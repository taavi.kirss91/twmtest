export enum ESortKey {
  NAME = "firstname",
  SURNAME = "surname",
  SEX = "sex",
  DATE= "date",
  PHONE= "phone",
}
