export interface IArticle {
  id: string;
  title: string;
  author: string;
  date: number;
  images: {[key: string]: string}[];
  intro: string;
  body: string;
  tags: string[];
}
