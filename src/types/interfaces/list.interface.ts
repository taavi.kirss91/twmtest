export interface IList {
  [key: string]: any;
  list: IListItem[];
}

export interface IListItem {
  author: string;
  body: string;
  boolean: boolean;
  date: number;
  email: string;
  firstname: string;
  id: string;
  image: {[key: string]: string};
  images: {[key: string]: string}[];
  intro: string;
  personal_code: number;
  phone: string;
  surname: string;
  sex: string;
  tags: string[];
  title: string;
}
