import { ArticleService } from "../api/article.service";
import { ListService } from "../api/list.service";

export class DI {
  private static _articleService: ArticleService;
  private static _listService: ListService;

  public static getArticleService(): ArticleService {
    return this._articleService || (this._articleService = new ArticleService());
  }

  public static getListService(): ListService {
    return this._listService || (this._listService = new ListService());
  }
}
