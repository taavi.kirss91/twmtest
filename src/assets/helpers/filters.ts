export function stripTags(input: string): string {
  const doc = new DOMParser().parseFromString(input, "text/html");
  return doc.body.textContent || "";
}

export function truncate(value: string, length: number): string {
  if (value.length <= length) {
    return value;
  } else {
    return value.slice(0, length) + "…";
  }
}
