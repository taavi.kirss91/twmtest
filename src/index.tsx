import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import { PageMain } from './components/pages/page-main/page-main';
import { BrowserRouter } from "react-router-dom";

ReactDOM.render(
  <BrowserRouter>
    <PageMain />
  </BrowserRouter>,
  document.getElementById('root')
);
